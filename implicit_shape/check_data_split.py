import numpy as np
import json
import config
import os

with open(config.path['data_split_json_path'], 'r') as split_file:
	splits = json.load(split_file)

train_split = splits['train']
val_split = splits['val']
test_split = splits['test']

dataset_path = config.path['src_dataset_path']
points_path = config.path['src_pt_path']
synsets = [s for s in os.listdir(dataset_path) if not s.endswith('json')]

# Check num synsets in split
assert len(list(train_split.keys())) == len(synsets)
assert len(list(val_split.keys())) == len(synsets)
assert len(list(test_split.keys())) == len(synsets)
out_dict_img = {}
out_dict_points = {}

for synset in synsets:
	syn_path = os.path.join(dataset_path, synset)
	objs = [obj for obj in os.listdir(syn_path) if not obj.endswith('lst')]
	import pdb; pdb.set_trace()
	# objs in split not in img_data
	not_train = [obj for obj in train_split[synset] if not obj in objs]
	not_val = [obj for obj in val_split[synset] if not obj in objs]
	not_test = [obj for obj in test_split[synset] if not obj in objs]
	not_img = np.concatenate([not_train, not_val, not_test], axis=0)
	out_dict_img[synset] = not_img

for synset in synsets:
	syn_path = os.path.join(points_path, synset, '4_pointcloud')
	objs = [obj for obj in os.listdir(syn_path)]
	# objs in split not in points data
	not_train = [obj for obj in train_split[synset] if not obj in objs]
	not_val = [obj for obj in val_split[synset] if not obj in objs]
	not_test = [obj for obj in test_split[synset] if not obj in objs]
	not_points = np.concatenate([not_train, not_val, not_test], axis=0)
	out_dict_points[synset] = not_points

print(out_dict_img)
print('----------')
print(out_dict_points)




