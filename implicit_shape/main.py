import torch
import torch.optim as optim
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import os
import sys
import torch.multiprocessing as mp
import subprocess
import config
from datetime import datetime
import utils
from dataloader import Dataset
from model import OccupancyNetwork
from tqdm import tqdm

# mp.set_sharing_strategy("file system")

def main():
    torch.backends.cudnn.benchmark = True

    # log params
    log_dir = config.logging['log_dir']
    exp_name = config.logging['exp_name']
    date = datetime.now().date().strftime("%m_%d_%Y")
    log_dir = os.path.join(log_dir, exp_name, date)
    os.makedirs(log_dir, exist_ok=True)
    utils.writelogfile(log_dir)

    # output directory
    out_dir = config.training['out_dir']
    
    os.makedirs(out_dir, exist_ok=True)

    batch_size = config.training['batch_size']
    batch_size_eval = config.training['batch_size_eval']
    num_epochs = config.training['num_epochs']

    save_model_step = config.training['save_model_step']
    eval_step = config.training['eval_step']
    verbose_step = config.training['verbose_step']

    num_points = config.training['num_points']
    # import pdb; pdb.set_trace()
    # Dataset
    print('Loading data...')
    train_dataset = Dataset(num_points=num_points, mode='train')
    eval_train_dataset = Dataset(mode='train')
    val_dataset = Dataset(mode='val')

    train_loader = torch.utils.data.DataLoader(
        train_dataset, batch_size=batch_size, num_workers=6, shuffle=True,pin_memory=True)
    eval_train_loader = torch.utils.data.DataLoader(
        eval_train_dataset, batch_size=batch_size_eval, num_workers=6,drop_last=True,pin_memory=True)
    val_loader = torch.utils.data.DataLoader(
        val_dataset, batch_size=batch_size_eval, num_workers=6,drop_last=True,pin_memory=True)

    # Model
    print('Initializing network...')
    model = OccupancyNetwork()

    # Initialize training
    optimizer = optim.Adam(model.parameters(), lr=1e-4)
    criterion = nn.BCEWithLogitsLoss()
    epoch_it = 0

    # Data parallel
    model = torch.nn.DataParallel(model).cuda()
    # model = model.cuda()

    print('Start training...')
    while True:
        # import pdb; pdb.set_trace()
        epoch_it += 1
        if num_epochs is not None and epoch_it > num_epochs:
            break
        print("Starting epoch %s"%(epoch_it))
        model = train(model, criterion, optimizer, train_loader, batch_size, epoch_it)

        if epoch_it % verbose_step == 0:
            print("Evaluating on train data...")
            mean_loss, mean_iou = eval(model, criterion, optimizer, eval_train_loader, batch_size, epoch_it)
            print('Mean loss on train set: %.4f'%(mean_loss))
            print('Mean IoU on train set: %.4f'%(mean_iou))

            del mean_loss
            del mean_iou

        if epoch_it % eval_step == 0:
            print('Evaluating on test data...')
            mean_loss_val, mean_iou_val = eval(model, criterion, optimizer, val_loader, batch_size, epoch_it)
            print('Mean loss on val set: %.4f'%(mean_loss_val))
            print('Mean IoU on val set: %.4f'%(mean_iou_val))


    
def train(model, criterion, optimizer, train_loader, batch_size, epoch_it):
    model.train()
    # import pdb; pdb.set_trace()
    with tqdm(total=int(len(train_loader)), ascii=True) as pbar:
        for mbatch in train_loader:
            img_input, points_input, occupancies = mbatch
            img_input = Variable(img_input).cuda()

            points_input = Variable(points_input).cuda()
            occupancies = Variable(occupancies).cuda()

            optimizer.zero_grad()
            
            logits = model(points_input, img_input)

            loss = criterion(logits, occupancies)
            loss.backward()
            optimizer.step()

            tqdm.write('Loss %.4f'%(loss.item()))

            del loss
            
            pbar.update(1)
    return model


def eval(model, criterion, optimizer, loader, batch_size, epoch_it):
    model.eval()
    loss_collect = []
    iou_collect = []

    with tqdm(total=int(len(loader)), ascii=True) as pbar:
        with torch.no_grad():
            for mbatch in loader:
                img_input, points_input, occupancies = mbatch
                img_input = Variable(img_input).cuda()

                points_input = Variable(points_input).cuda()
                occupancies = Variable(occupancies).cuda()

                optimizer.zero_grad()

                logits = model(points_input, img_input)

                loss = criterion(logits, occupancies)

                # logtis_np = logits.data.cpu().numpy()
                loss_collect.append(loss.data.cpu().item())

                iou = utils.compute_iou(logits.detach().cpu().numpy(), \
                            occupancies.detach().cpu().numpy())
                iou_collect.append(iou)
                pbar.update(1)

    mean_loss = np.mean(np.array(loss_collect))
    mean_iou = np.mean(np.array(iou_collect))

    return mean_loss, mean_iou

if __name__ == '__main__':
    main()










