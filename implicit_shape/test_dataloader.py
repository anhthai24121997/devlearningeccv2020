import torch
from dataloader import Dataset
import config as config


dataset = Dataset(config)
dataset.load_image_paths()