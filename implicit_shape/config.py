path = dict(
		src_dataset_path = '/media/ant/5072f36a-8c4f-445d-89f5-577b5b1e05d9/ShapeNet_13_RGB_D+N',
		input_image_path = 'image_output',
		input_depth_path = 'depth_output',
		input_normal_path = 'normal_output',
		src_pt_path = '/media/ant/50b6d91a-50d7-45a7-b77c-92b79a62e56a/data/ShapeNet.build',
		input_points_path = '',
		input_pointcloud_path = '',
		input_metadata_path = '',
		data_split_json_path = '/media/ant/5072f36a-8c4f-445d-89f5-577b5b1e05d9/ShapeNet_13_RGB_D+N/data_split.json'
			)
data_setting = dict(
		input_size = 224,
		img_extension = 'png',
		random_view = True,
		seq_len = 25
		)
training = dict(
		out_dir = '/media/ant/50b6d91a-50d7-45a7-b77c-92b79a62e56a/onet_output/test',
		batch_size = 128,
		batch_size_eval = 10,
		num_epochs = None,
		save_model_step = 5,
		eval_step = 5,
		verbose_step = 100,
		num_points = 2048)
logging = dict(
		log_dir = '/media/ant/5072f36a-8c4f-445d-89f5-577b5b1e05d9/log',
		exp_name = 'Test'
		)

