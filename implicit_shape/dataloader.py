import numpy as np
import torch
from torch.utils.data import Dataset
from PIL import Image
import os
import glob
import config
import utils
import json
from torchvision import transforms

class Dataset(Dataset):
    def __init__(self, num_points=-1,mode='train'):

        self.mode = mode
        
        self.input_size = config.data_setting['input_size']
        self.num_points = num_points

        self.src_dataset_path = config.path['src_dataset_path']
        self.input_image_path = config.path['input_image_path']
        self.input_depth_path = config.path['input_depth_path']
        self.input_normal_path = config.path['input_normal_path']
        self.img_extension = config.data_setting['img_extension']


        self.src_pt_path = config.path['src_pt_path']
        self.input_points_path = config.path['input_points_path']
        self.input_pointcloud_path = config.path['input_pointcloud_path']

        self.input_metadata_path = config.path['input_metadata_path']

        self.data_split_json_path = config.path['data_split_json_path']


        with open(self.data_split_json_path, 'r') as data_split_file:
            self.data_splits = json.load(data_split_file)
        self.split = self.data_splits[self.mode]
        self.random_view = config.data_setting['random_view']
        self.seq_len = config.data_setting['seq_len']

        self.catnames = sorted(list(self.split.keys()))

        self.obj_cat_map = [(obj,cat) for cat in self.catnames \
                                for obj in self.split[cat]]
        self.img_paths = [os.path.join(self.src_dataset_path, cat, obj) \
                                for (obj, cat) in self.obj_cat_map]
        self.points_split_paths = [os.path.join(self.src_pt_path, cat, \
                        '4_points', obj) for (obj, cat) in self.obj_cat_map]
        self.metadata_split_paths = [os.path.join(self.src_dataset_path, cat, \
                        obj, 'metadata.txt') \
                            for (obj, cat) in self.obj_cat_map]

        self.img_transform = transforms.Compose([
            transforms.Resize(self.input_size),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
        ])
        self.image_split_paths, self.depth_split_paths, self.normal_split_paths \
            = self.load_image_paths()

    def load_image_paths(self):
        assert self.input_image_path is not None

        image_split_files = [sorted(glob.glob(
                    os.path.join(obj_path, self.input_image_path, '*.%s' 
                        % self.img_extension)))[:-1] \
                            for obj_path in self.img_paths]

        if self.input_depth_path is not None:
            depth_split_files = [sorted(glob.glob(
                    os.path.join(obj_path, self.input_depth_path, '*.%s' 
                        % self.img_extension)))[:-1] \
                            for obj_path in self.img_paths]
        else:
            depth_split_files = None
        if self.input_normal_path is not None:
            normal_split_files = [sorted(glob.glob(
                    os.path.join(obj_path, self.input_normal_path, '*.%s' 
                        % self.img_extension)))[:-1] \
                            for obj_path in self.img_paths]
        else:
            normal_split_files = None


        return image_split_files, depth_split_files, normal_split_files

    def get_data_sample(self, index, img_idx=-1):
        # print('Get image data')
        if self.random_view:
            assert img_idx != -1

        else:
            idx = index//self.seq_len
            img_idx = index % self.seq_len

            index = idx

        input_image = self.image_split_paths[index][img_idx]
        input_depth = self.depth_split_paths[index][img_idx]
        input_normal = self.normal_split_paths[index][img_idx]
        image_data = Image.open(input_image).convert('RGB')
        image_data = self.img_transform(image_data)
        # image_data = utils.normalize_imagenet(image_data)
        image_data = np.array(image_data.numpy())
        
        if self.depth_split_paths is not None:
            depth_data = Image.open(input_depth).convert('L')
            depth_data = depth_data.resize((self.input_size, self.input_size))
            depth_data = np.array(depth_data)/255.
            depth_data = np.expand_dims(depth_data, axis=2)
            depth_data = depth_data.transpose(2,0,1)
            image_data = np.concatenate(\
                [image_data, depth_data],axis=0)

        else:
            depth_data = None
        if self.normal_split_paths is not None:
            normal_data = Image.open(input_normal).convert('RGB')
            normal_data = normal_data.resize((self.input_size, self.input_size))
            normal_data = np.array(normal_data)/255.
            normal_data = normal_data.transpose(2,0,1)
            image_data = np.concatenate(\
                [image_data, normal_data],axis=0)
        else:
            normal_data = None

        image_data = torch.FloatTensor(image_data)

        return image_data


    def get_points_sample(self, index, img_idx=-1):
        # print('Get points and occs')
        if self.random_view:
            assert img_idx != -1
        else:
            idx = index//self.seq_len
            img_idx = index % self.seq_len

            index = idx
        input_points_path = self.points_split_paths[index]
        input_points = np.load(\
                        os.path.join(input_points_path,'points.npz'),mmap_mode='r')\
                            ['points']
        input_occs = np.load(\
                        os.path.join(input_points_path, 'occupancies.npz'),mmap_mode='r')\
                            ['occupancies']
        input_occs = np.unpackbits(input_occs)
        input_occs = input_occs.astype(np.float32)

        input_points, input_occs = utils.sample_points(input_points,\
                                                   input_occs,\
                                                   self.num_points)


        input_metadata_path = self.metadata_split_paths[index]
        meta = np.loadtxt(input_metadata_path)
        rotate_dict = {'elev': meta[img_idx][1], 'azim': meta[img_idx][0]}

        input_points = utils.apply_rotate(input_points, rotate_dict)
        input_points = torch.FloatTensor(input_points)
        input_occs = torch.FloatTensor(input_occs)
        return input_points, input_occs

    def get_pointcloud_sample(self, index, img_idx=-1):
        pass


    def __getitem__(self, index):
        if self.random_view:
            img_idx = np.random.choice(self.seq_len)
        else:
            img_idx = -1
        image_data = self.get_data_sample(index, img_idx)
        points_data, occs_data = self.get_points_sample(index, img_idx)
        # points_data, occs_data = torch.FloatTensor(np.arange(32)), torch.FloatTensor(np.arange(32))
        return image_data, points_data, occs_data

    def __len__(self):
        if self.random_view:
            return len(self.image_split_paths)
        return len(self.image_split_paths)*self.seq_len
        