import os
import json
import shutil

target_path = '/media/ant/5072f36a-8c4f-445d-89f5-577b5b1e05d9/ShapeNet_13_RGB_D+N'

split_json_path = '/media/ant/5072f36a-8c4f-445d-89f5-577b5b1e05d9/ShapeNet_13_RGB_D+N/data_split.json'

synsets = os.listdir(target_path)

sub_dict = {}
# import pdb; pdb.set_trace()
for split in ['train', 'val', 'test', 'train_sub', 'train_sub_75']:
    sub_dict[split] = {}
    for synset in synsets:
        lst_path = os.path.join(target_path, synset, '{}.lst'.format(split))

        with open(lst_path, 'r') as f:
            lines = f.readlines()
        
        lines = [x.replace('\n', '') for x in lines]

        sub_dict[split][synset] = lines


output = json.dumps(sub_dict, indent = 4)
with open(split_json_path, 'w') as f:
    f.write(output)
'''
split = 'val'
sub_dict[split] = {}
for synset in synsets:
    lst_path = os.path.join(target_path, synset, '{}.lst'.format(split))

    with open(lst_path, 'r') as f:
        lines = f.readlines()
    
    lines = [x.replace('\n', '') for x in lines]

    sub_dict[synset] = lines

output = json.dumps(sub_dict, indent = 4)

# with open('{}_unseen_toys45k.json'.format(split), 'w') as f:
#     f.write(output)

split = 'test'
for synset in synsets:
    lst_path = os.path.join(target_path, synset, '{}.lst'.format(split))

    with open(lst_path, 'r') as f:
        lines = f.readlines()
    
    lines = [x.replace('\n', '') for x in lines]

    sub_dict[synset] = lines

output = json.dumps(sub_dict, indent = 4)

with open('{}_unseen_toys45k.json'.format(split), 'w') as f:
    f.write(output)

split = 'train'
for synset in synsets:
    lst_path = os.path.join(target_path, synset, '{}.lst'.format(split))

    with open(lst_path, 'r') as f:
        lines = f.readlines()
    
    lines = [x.replace('\n', '') for x in lines]

    sub_dict[synset] = lines

output = json.dumps(sub_dict, indent = 4)

with open('{}_unseen_toys45k.json'.format(split), 'w') as f:
    f.write(output)
'''
