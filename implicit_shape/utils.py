import numpy as np
import os
import config
from datetime import datetime
import copy
import torch

def writelogfile(log_dir):
    log_file_name = os.path.join(log_dir, 'log.txt')
    with open(log_file_name, "a+") as log_file:
        log_string = get_log_string()
        log_file.write(log_string)


def get_log_string():
    now = str(datetime.now().strftime("%H:%M %d-%m-%Y"))
    log_string = ""
    log_string += " -------- Hyperparameters and settings -------- \n"
    log_string += "{:25} {}\n".format('Time:', now)
    log_string += "{:25} {}\n".format('Mini-batch size:', \
        config.training['batch_size'])
    log_string += "{:25} {}\n".format('Batch size eval:', \
        config.training['batch_size_eval'])
    log_string += "{:25} {}\n".format('Num epochs:', \
        config.training['num_epochs'])
    log_string += "{:25} {}\n".format('Out directory:', \
        config.training['out_dir'])
    log_string += "{:25} {}\n".format('Random view:', \
        config.data_setting['random_view'])
    log_string += "{:25} {}\n".format('Sequence length:', \
        config.data_setting['seq_len'])
    log_string += "{:25} {}\n".format('Input size:', \
        config.data_setting['input_size'])
    log_string += " -------- Data paths -------- \n"
    log_string += "{:25} {}\n".format('Dataset path', \
        config.path['src_dataset_path'])
    log_string += "{:25} {}\n".format('Point path', \
        config.path['src_pt_path'])
    log_string += " ------------------------------------------------------ \n"
    return log_string




def compute_iou(occ1, occ2):
    ''' Computes the Intersection over Union (IoU) value for two sets of
    occupancy values.

    Args:
        occ1 (tensor): first set of occupancy values
        occ2 (tensor): second set of occupancy values
    '''
    occ1 = np.asarray(occ1)
    occ2 = np.asarray(occ2)

    # Put all data in second dimension
    # Also works for 1-dimensional data
    if occ1.ndim >= 2:
        occ1 = occ1.reshape(occ1.shape[0], -1)
    if occ2.ndim >= 2:
        occ2 = occ2.reshape(occ2.shape[0], -1)

    # Convert to boolean values
    occ1_temp = copy.deepcopy(occ1)
    occ2_temp = copy.deepcopy(occ2)
    occ1 = (occ1 >= 0.5)
    occ2 = (occ2 >= 0.5)

    # Compute IOU
    area_union = (occ1 | occ2).astype(np.float32).sum(axis=-1)
    if (area_union == 0).any():
        import pdb; pdb.set_trace()

    area_intersect = (occ1 & occ2).astype(np.float32).sum(axis=-1)

    iou = (area_intersect / area_union)

    return iou

def apply_rotate(input_points, rotate_dict):
    theta_azim = rotate_dict['azim']
    theta_elev = rotate_dict['elev']
    theta_azim = np.pi+theta_azim/180*np.pi
    theta_elev = theta_elev/180*np.pi
    r_elev = np.array([[1,       0,          0],
                        [0, np.cos(theta_elev), -np.sin(theta_elev)],
                        [0, np.sin(theta_elev), np.cos(theta_elev)]])
    r_azim = np.array([[np.cos(theta_azim), 0, np.sin(theta_azim)],
                        [0,               1,       0],
                        [-np.sin(theta_azim),0, np.cos(theta_azim)]])

    rotated_points = r_elev@r_azim@input_points.T
    return rotated_points.T

def sample_points(input_points, input_occs, num_points):
    if num_points != -1:
        idx = torch.randint(len(input_points), size=(num_points,))
    else:
        idx = torch.arange(len(input_points))
    selected_points = input_points[idx, :]
    selected_occs = input_occs[idx]
    return selected_points, selected_occs

def normalize_imagenet(x):
    ''' Normalize input images according to ImageNet standards.
    Args:
        x (tensor): input images
    '''
    x = x.clone()
    x[:, 0] = (x[:, 0] - 0.485) / 0.229
    x[:, 1] = (x[:, 1] - 0.456) / 0.224
    x[:, 2] = (x[:, 2] - 0.406) / 0.225
    return x